# Back Start

该项目为**Unity NewbiesJam**参赛项目，意为**回到起点**。

游戏方法：通过**WASD**控制移动，从起点出发吃到**星星**后返回起点即为通关。

**已上传TapTap：**https://www.taptap.cn/developer/247082

> 本次GameJam主题为**End where you started**，开发时间为一周，从10月14日16:00主题发布开始，到10月21日16:00提交截止。


![back-start-1](https://back-start-1258865037.cos.ap-beijing.myqcloud.com/back-start-1.gif)

![back-start-2](https://back-start-1258865037.cos.ap-beijing.myqcloud.com/back-start-2.gif)

