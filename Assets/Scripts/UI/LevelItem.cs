using System;
using System.Collections;
using System.Collections.Generic;
using EchoFramework;
using UnityEngine;
using UnityEngine.UI;

public class LevelItem : MonoBehaviour
{
    [SerializeField]
    private Button levelBtn;

    [SerializeField]
    private Image passImg;

    [SerializeField]
    private Image lockImg;

    [SerializeField]
    private Image unLockImg;

    [SerializeField]
    private Text passTxt;

    [SerializeField]
    private Text locktxt;

    [SerializeField]
    private Text unLockTxt;

    /// <summary>
    /// 关卡Item数据
    /// </summary>
    private LevelItemData levelItemData;
    
    private void Start()
    {
        levelBtn.onClick.AddListener(OnClickLevelBtn);
    }

    /// <summary>
    /// 设置数据
    /// </summary>
    /// <param name="data">关卡数据</param>
    public void SetData(LevelItemData data)
    {
        levelItemData = data;
        Refresh();
    }

    /// <summary>
    /// 刷新Item
    /// </summary>
    private void Refresh()
    {
        if (levelItemData == null)
        {
            Debug.LogError("关卡数据为Null！");
            return;
        }

        passTxt.text = levelItemData.Level.ToString();
        locktxt.text = levelItemData.Level.ToString();
        unLockTxt.text = levelItemData.Level.ToString();
        passImg.gameObject.SetActive(levelItemData.LevelStatus == ELevelStatus.Pass);
        unLockImg.gameObject.SetActive(levelItemData.LevelStatus == ELevelStatus.Lock);
        lockImg.gameObject.SetActive(levelItemData.LevelStatus == ELevelStatus.UnLock);
        levelBtn.interactable = levelItemData.LevelStatus != ELevelStatus.Lock;
    }
    
    /// <summary>
    /// 点击关卡
    /// </summary>
    private void OnClickLevelBtn()
    {
        if (levelItemData == null)
        {
            Debug.LogError("关卡数据为Null！");
            return;
        }

        if (levelItemData.LevelStatus == ELevelStatus.Lock)
        {
            Debug.LogWarning("当前关卡未解锁");
            return;
        }
        
        UIViewManager.Instance.OpenUIView(EUIViewID.GameView);
        LevelManager.Instance.LoadLevel(levelItemData.Level);
        EventCenter.Instance.Execute(EventType.CloseSelectView);
        GameManager.Instance.PlayNormalBtnClip();
    }
}