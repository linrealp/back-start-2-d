using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIViewManager : MonoBehaviour
{
    private static UIViewManager instance;
    public static UIViewManager Instance => instance;

    [SerializeField]
    private GameObject mainMenuView;

    [SerializeField]
    private GameObject settingsView;

    [SerializeField]
    private GameObject selectLevelView;

    [SerializeField]
    private GameObject gameView;

    [SerializeField]
    private GameObject victoryView;

    [SerializeField]
    private GameObject introduceView;
    
    [SerializeField]
    private GameObject thanksView;
    
    [SerializeField]
    private GameObject privacyView;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        OpenUIView(EUIViewID.MainMenuView);
    }

    /// <summary>
    /// 打开对应UI界面
    /// </summary>
    /// <param name="viewID">界面ViewID</param>
    public void OpenUIView(EUIViewID viewID)
    {
        switch (viewID)
        {
            case EUIViewID.MainMenuView:
                Instantiate(mainMenuView, transform);
                break;
            case EUIViewID.SettingsView:
                Instantiate(settingsView, transform);
                break;
            case EUIViewID.SelectLevelView:
                Instantiate(selectLevelView, transform);
                break;
            case EUIViewID.GameView:
                if (!GameView.IsOpen)
                    Instantiate(gameView, transform);
                break;
            case EUIViewID.VictoryView:
                Instantiate(victoryView, transform);
                break;
            case EUIViewID.IntroduceView:
                Instantiate(introduceView, transform);
                break;
            case EUIViewID.ThanksView:
                Instantiate(thanksView, transform);
                break; 
            case EUIViewID.PrivacyView:
                Instantiate(privacyView, transform);
                break;
        }
    }
}