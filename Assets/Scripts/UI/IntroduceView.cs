using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 介绍界面
/// </summary>
public class IntroduceView : MonoBehaviour
{
    [SerializeField]
    private Button closeBtn;
    private void Start()
    {
        closeBtn.onClick.AddListener(OnClickCloseBtn);
    }

    /// <summary>
    /// 点击关闭按钮
    /// </summary>
    private void OnClickCloseBtn()
    {
        Destroy(gameObject);
    }
}
