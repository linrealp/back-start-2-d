using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 致谢界面
/// </summary>
public class ThanksView : MonoBehaviour
{
    private void Awake()
    {
        Button repoBtn = transform.Find("Panel/linkTxtBtn").GetComponent<Button>();
        RegisterBtn(repoBtn);

        Button closeBtn = transform.Find("CloseBtn").GetComponent<Button>();
        closeBtn.onClick.AddListener(() => Destroy(this.gameObject));

        for (int i = 0; i <= 4; i++)
        {
            Button tempBtn = transform.Find($"linkTxtBtn ({i})").GetComponent<Button>();
            RegisterBtn(tempBtn);
        }
    }

    private void RegisterBtn(Button btn)
    {
        Text tempTxt = btn.GetComponent<Text>();
        btn.onClick.AddListener(() => Application.OpenURL(tempTxt.text));
    }
}