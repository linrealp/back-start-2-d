using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuView : MonoBehaviour
{
    [SerializeField]
    private Button playBtn;

    [SerializeField]
    private Button settingBtn;

    [SerializeField]
    private Button exitBtn;

    [SerializeField]
    private Button questBtn;

    [SerializeField]
    private Button thanksBtn;

    [SerializeField]
    private Button privacyBtn;

    private void Start()
    {
        playBtn.onClick.AddListener(OnClickPlayBtn);
        settingBtn.onClick.AddListener(OnClickSettingBtn);
        exitBtn.onClick.AddListener(OnClickExitBtn);
        questBtn.onClick.AddListener(OnClickQuestBtn);
        thanksBtn.onClick.AddListener(OnClickThanksBtn);
        privacyBtn.onClick.AddListener(OnClickPrivacyBtn);
        
        GameManager.Instance.PlayerMainMenuBGM();
    }

    private void OnClickPrivacyBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        UIViewManager.Instance.OpenUIView(EUIViewID.PrivacyView);
    }

    /// <summary>
    /// 点击开始游戏按钮
    /// </summary>
    private void OnClickPlayBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        UIViewManager.Instance.OpenUIView(EUIViewID.SelectLevelView);
        GameManager.Instance.CreateLevelGenerator();
        Destroy(gameObject);
    }

    /// <summary>
    /// 点击设置游戏按钮
    /// </summary>
    private void OnClickSettingBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        UIViewManager.Instance.OpenUIView(EUIViewID.SettingsView);
    }

    /// <summary>
    /// 点击退出按钮
    /// </summary>
    private void OnClickExitBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    /// <summary>
    /// 点击问号按钮
    /// </summary>
    private void OnClickQuestBtn()
    {
        UIViewManager.Instance.OpenUIView(EUIViewID.IntroduceView);
        GameManager.Instance.PlayNormalBtnClip();
    }
    
    /// <summary>
    /// 点击致谢按钮
    /// </summary>
    private void OnClickThanksBtn()
    {
        UIViewManager.Instance.OpenUIView(EUIViewID.ThanksView);
        GameManager.Instance.PlayNormalBtnClip();
    }
}