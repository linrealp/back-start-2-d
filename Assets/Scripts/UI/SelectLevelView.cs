using System;
using System.Collections;
using System.Collections.Generic;
using EchoFramework;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 选择关卡界面
/// </summary>
public class SelectLevelView : MonoBehaviour
{
    [SerializeField]
    private Button closeBtn;

    [SerializeField]
    private GridLayoutGroup gridGroup;

    [SerializeField]
    private GameObject levelItemObj;

    private void Start()
    {
        closeBtn.onClick.AddListener(OnClickCloseBtn);
        InitLevelItem();
        EventCenter.Instance.Register(EventType.CloseSelectView, CloseSelectView);
        GameManager.Instance.PlayerMainMenuBGM();
    }


    /// <summary>
    /// 关闭界面
    /// </summary>
    private void CloseSelectView(EventObject eo)
    {
        Destroy(gameObject);
    }

    /// <summary>
    /// 点击关闭按钮  会返回主菜单
    /// </summary>
    private void OnClickCloseBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        UIViewManager.Instance.OpenUIView(EUIViewID.MainMenuView);
        Destroy(gameObject);
    }

    /// <summary>
    /// 生成关卡Item
    /// </summary>
    private void InitLevelItem()
    {
        int levelCnt = LevelManager.Instance.MaxLevelCnt;
        for (int i = 0; i < levelCnt; i++)
        {
            GameObject obj = Instantiate(levelItemObj, gridGroup.transform);
            LevelItem item = obj.GetComponent<LevelItem>();
            item.SetData(new LevelItemData(i + 1));
        }
    }
    
    private void OnDestroy()
    {
        EventCenter.Instance.UnRegiseterByAction(EventType.CloseSelectView, CloseSelectView);
    }
}