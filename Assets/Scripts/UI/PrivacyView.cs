using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 隐私条款界面
/// </summary>
public class PrivacyView : MonoBehaviour
{
    private void Awake()
    {
        Button closeBtn = transform.Find("CloseBtn").GetComponent<Button>();
        closeBtn.onClick.AddListener(() => Destroy(this.gameObject));
    }

    private void RegisterBtn(Button btn)
    {
        Text tempTxt = btn.GetComponent<Text>();
        btn.onClick.AddListener(() => Application.OpenURL(tempTxt.text));
    }
}
