using System;
using EchoFramework;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 游戏运行时界面
/// </summary>
public class GameView : MonoBehaviour
{
    [SerializeField]
    private Button restartBtn;
    [SerializeField]
    private Button settingBtn;
    [SerializeField]
    private Button exitBtn;

    [SerializeField]
    private Text levelTxt;

    public static bool IsOpen;

    private void Awake()
    {
        EventCenter.Instance.Register(EventType.UpdateLevelTxt, UpdateLevelTxt);
    }

    private void Start()
    {
        restartBtn.onClick.AddListener(OnClickRestartBtn);
        settingBtn.onClick.AddListener(OnClickSettingBtn);
        exitBtn.onClick.AddListener(OnClickExitBtn);
        IsOpen = true;
        GameManager.Instance.PlayerGameBGM();
    }

    /// <summary>
    /// 点击重开按钮
    /// </summary>
    private void OnClickRestartBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        LevelManager.Instance.LoadLevel(LevelManager.Instance.CurLevel);
    }
    
    /// <summary>
    /// 点击设置按钮
    /// </summary>
    private void OnClickSettingBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        UIViewManager.Instance.OpenUIView(EUIViewID.SettingsView);
    }

    /// <summary>
    /// 点击退出按钮
    /// </summary>
    private void OnClickExitBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        UIViewManager.Instance.OpenUIView(EUIViewID.SelectLevelView);
        Destroy(gameObject);
    }

    /// <summary>
    /// 更新level文字
    /// </summary>
    private void UpdateLevelTxt(EventObject eventObject)
    {
        levelTxt.text = "关卡  " + LevelManager.Instance.CurLevel;
    }
    
    private void OnDestroy()
    {
        EventCenter.Instance.UnRegiseterByAction(EventType.UpdateLevelTxt, UpdateLevelTxt);
        IsOpen = false;
    }
}
