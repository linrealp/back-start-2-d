using UnityEngine;
using UnityEngine.UI;

public class SettingsView : MonoBehaviour
{
    [SerializeField]
    private Slider musicSlider;

    [SerializeField]
    private Slider soundSlider;

    [SerializeField]
    private Button closeBtn;

    private void Start()
    {
        musicSlider.onValueChanged.AddListener(OnMusicSliderChange);
        soundSlider.onValueChanged.AddListener(OnSoundSliderChange);
        closeBtn.onClick.AddListener(OnClickCloseBtn);
        
        float bgmVolume = PlayerPrefs.GetFloat("BgmVolume", 0.2f);
        float soundVolume = PlayerPrefs.GetFloat("SoundVolume", 0.2f);
        musicSlider.value = bgmVolume;
        soundSlider.value = soundVolume;
    }

    /// <summary>
    /// 背景音乐改变
    /// </summary>
    /// <param name="changevalue">参数</param>
    private void OnMusicSliderChange(float changevalue)
    {
        GameManager.Instance.SetBgmVolume(changevalue);
    }

    /// <summary>
    /// 音效改变
    /// </summary>
    /// <param name="changeValue">参数</param>
    private void OnSoundSliderChange(float changeValue)
    {
        GameManager.Instance.SetSoundVolume(changeValue);
    }

    /// <summary>
    /// 关闭界面
    /// </summary>
    private void OnClickCloseBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        GameManager.Instance.SaveMusciSetting();
        Destroy(gameObject);
    }
}