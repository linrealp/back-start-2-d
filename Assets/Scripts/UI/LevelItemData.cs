﻿public class LevelItemData
{
    /// <summary>
    /// 关卡
    /// </summary>
    public int Level { get; private set; }

    /// <summary>
    /// 关卡状态
    /// </summary>
    public ELevelStatus LevelStatus => GameManager.Instance.GetLevelStatus(Level);

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="level">当前关卡</param>
    public LevelItemData(int level)
    {
        Level = level;
    }
}