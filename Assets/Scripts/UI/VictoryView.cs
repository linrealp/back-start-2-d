using System;
using System.Collections;
using System.Collections.Generic;
using EchoFramework;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 通关界面
/// </summary>
public class VictoryView : MonoBehaviour
{
    [SerializeField]
    private Text stageClearTxt;

    [SerializeField]
    private Text victoryTxt;

    [SerializeField]
    private Button closeBtn;

    [SerializeField]
    private Button homeBtn;

    [SerializeField]
    private Button nextLevelBtn;

    [SerializeField]
    private Button restartBtn;

    [SerializeField]
    private Button exitBtn;

    private void Start()
    {
        nextLevelBtn.gameObject.SetActive(LevelManager.Instance.CurLevel < LevelManager.Instance.MaxLevelCnt);
        homeBtn.gameObject.SetActive(LevelManager.Instance.CurLevel == LevelManager.Instance.MaxLevelCnt);
        stageClearTxt.gameObject.SetActive(LevelManager.Instance.CurLevel == LevelManager.Instance.MaxLevelCnt);
        victoryTxt.gameObject.SetActive(LevelManager.Instance.CurLevel < LevelManager.Instance.MaxLevelCnt);
        
        closeBtn.onClick.AddListener(OnClickCloseBtn);
        homeBtn.onClick.AddListener(OnClickHomeBtn);
        nextLevelBtn.onClick.AddListener(OnClickNextLevelBtn);
        restartBtn.onClick.AddListener(OnClickRestartBtn);
        exitBtn.onClick.AddListener(OnClickExitBtn);
        closeBtn.gameObject.SetActive(false);
        
        GameManager.Instance.PlayVictoryClip();
    }


    /// <summary>
    /// 点击关闭按钮
    /// </summary>
    private void OnClickCloseBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        Destroy(gameObject);
    }

    /// <summary>
    /// 点击首页按钮
    /// </summary>
    private void OnClickHomeBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        EventCenter.Instance.Execute(EventType.CloseLevelManager);
        UIViewManager.Instance.OpenUIView(EUIViewID.MainMenuView);
        Destroy(gameObject);
    }

    /// <summary>
    /// 点击下一关按钮
    /// </summary>
    private void OnClickNextLevelBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        if(LevelManager.Instance.CurLevel < LevelManager.Instance.MaxLevelCnt)
            LevelManager.Instance.LoadLevel(LevelManager.Instance.CurLevel + 1);
        Destroy(gameObject);
    }

    /// <summary>
    /// 点击重开按钮
    /// </summary>
    private void OnClickRestartBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        LevelManager.Instance.LoadLevel(LevelManager.Instance.CurLevel);
        Destroy(gameObject);
    }

    /// <summary>
    /// 点击退出按钮
    /// </summary>
    private void OnClickExitBtn()
    {
        GameManager.Instance.PlayNormalBtnClip();
        UIViewManager.Instance.OpenUIView(EUIViewID.SelectLevelView);
        Destroy(gameObject);
    }
}