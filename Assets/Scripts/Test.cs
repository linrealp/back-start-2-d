using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Test : MonoBehaviour
{
    public GameObject item;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Quaternion rotation = item.transform.localRotation;
            Vector3 target = new Vector3(rotation.eulerAngles.x, rotation.eulerAngles.y, rotation.eulerAngles.z - 90f);
            item.transform.DOLocalRotate(target, 1f).OnComplete(() => item.transform.localRotation = new Quaternion(0, 0, 0, rotation.w));
        }
    }
}