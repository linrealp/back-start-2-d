﻿/// <summary>
/// 摄像机配置
/// </summary>
public struct CameraConfig
{
    /// <summary>
    /// 摄像机x轴
    /// </summary>
    public float X
    {
        get;
        private set;
    }
    /// <summary>
    /// 摄像机Y轴
    /// </summary>
    public float Y
    {
        get;
        private set;
    }

    /// <summary>
    /// 摄像机Orthgraphic Size
    /// </summary>
    public float Size
    {
        get;
        private set;
    }

    /// <summary>
    /// 背景图x轴
    /// </summary>
    public float BgX
    {
        get;
        private set;
    }

    /// <summary>
    /// 背景图y轴
    /// </summary>
    public float BgY
    {
        get;
        private set;
    }

    /// <summary>
    /// 背景图x缩放
    /// </summary>
    public float BgScaleX
    {
        get;
        private set;
    }

    /// <summary>
    /// 背景图y轴缩放
    /// </summary>
    public float BgScaleY
    {
        get;
        private set;
    }

    public CameraConfig(float varX, float varY, float varSize, float varBgX, float varBgY, float varBgScaleX, float varBgScaleY)
    {
        X = varX;
        Y = varY;
        Size = varSize;
        BgX = varBgX;
        BgY = varBgY;
        BgScaleX = varBgScaleX;
        BgScaleY = varBgScaleY;
    }
}