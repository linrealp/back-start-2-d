using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ArrowItem : MonoBehaviour
{
    /// <summary>
    /// 箭头序列，与枚举对应
    /// </summary>
    [SerializeField]
    private GameObject[] arrowDirObjs;


    /// <summary>
    /// 设置所有箭头显示
    /// </summary>
    /// <param name="isShow"></param>
    private void SetAllShow(bool isShow)
    {
        foreach (var obj in arrowDirObjs)
        {
            if(obj == null)
                continue;
            obj.SetActive(isShow);
        }
    }
    
    /// <summary>
    /// 设置箭头方向 自动更新显示
    /// </summary>
    /// <param name="direction">方向</param>
    public void SetData(EDirection direction)
    {
        SetAllShow(false);
        if (direction == EDirection.None)
            return;
        
        arrowDirObjs[(int)direction].SetActive(true);
    }
}
