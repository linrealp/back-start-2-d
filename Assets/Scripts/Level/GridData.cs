﻿using UnityEngine;

/// <summary>
/// 单位格子
/// </summary>
public class GridData
{
    /// <summary>
    /// X坐标
    /// </summary>
    public int X
    {
        get;
        private set;
    }

    /// <summary>
    /// Y坐标
    /// </summary>
    public int Y
    {
        get; 
        private set;
    }

    public EGridType GridType
    {
        get;
        private set;
    }
    
    /// <summary>
    /// 颜色
    /// </summary>
    public EColor GridColor
    {
        get;
        private set;
    }

    /// <summary>
    /// 方向
    /// </summary>
    public EDirection GridDirection
    {
        get;
        set;
    }

    /// <summary>
    /// 开关按钮类型
    /// </summary>
    public ESwitchBtnType GridSwtichBtnType
    {
        get;
        private set;
    }

    /// <summary>
    /// 按钮是否可按下
    /// </summary>
    public bool IsBtnCanUse
    {
        get;
        private set;
    }

    /// <summary>
    /// 条件是否已获得
    /// </summary>
    public bool IsTargetFinish
    {
        get;
        set;
    }

    /// <summary>
    /// 构造函数  地面上物体类型(仅不包含开关)
    /// </summary>
    /// <param name="x">X坐标</param>
    /// <param name="y">y坐标</param>
    /// <param name="gridType">格子类型</param>
    /// <param name="color">颜色</param>
    /// <param name="direction">方向</param>
    public GridData(int x, int y, EGridType gridType, EColor color = EColor.None, EDirection direction = EDirection.None)
    {
        X = x;
        Y = y;
        GridType = gridType;
        GridColor = color;
        GridDirection = direction;
        if (GridType == EGridType.Target)
        {
            IsTargetFinish = false;
        }
    }
    
    /// <summary>
    /// 构造函数  地板表面物体，仅开关
    /// </summary>
    /// <param name="x">X坐标</param>
    /// <param name="y">y坐标</param>
    /// <param name="gridType">格子类型</param>
    /// <param name="switchBtnType">开关类型</param>
    /// <param name="color">颜色</param>
    public GridData(int x, int y, EGridType gridType, ESwitchBtnType switchBtnType, EColor color = EColor.None)
    {
        X = x;
        Y = y;
        GridType = gridType;
        GridSwtichBtnType = switchBtnType;
        GridColor = color;
        IsBtnCanUse = true;
    }
    
    /// <summary>
    /// 构造函数 克隆用
    /// </summary>
    /// <param name="x">X坐标</param>
    /// <param name="y">y坐标</param>
    /// <param name="gridType">格子类型</param>
    /// <param name="switchBtnType">开关类型</param>
    /// <param name="color">颜色</param>
    /// <param name="direction">方向</param>
    /// <param name="btnCanUse">按钮是否可用</param>
    /// <param name="isTargetFinsih">条件是否已获得</param>
    private GridData(int x, int y, EGridType gridType, ESwitchBtnType switchBtnType, EColor color, EDirection direction, 
        bool btnCanUse, bool isTargetFinsih)
    {
        X = x;
        Y = y;
        GridType = gridType;
        GridSwtichBtnType = switchBtnType;
        GridColor = color;
        GridDirection = direction;
        IsBtnCanUse = btnCanUse;
        IsTargetFinish = isTargetFinsih;
    }

    /// <summary>
    /// 获取克隆数据
    /// </summary>
    /// <returns></returns>
    public GridData GetCloneGirdData()
    {
        GridData gridData = new GridData(X, Y, GridType, GridSwtichBtnType, GridColor, GridDirection, IsBtnCanUse, IsTargetFinish);
        return gridData;
    }

    /// <summary>
    /// 设置开关是否可用
    /// </summary>
    /// <param name="canUse"></param>
    public void SetBtnUse(bool canUse)
    {
        if (GridSwtichBtnType == ESwitchBtnType.Single)
        {
            IsBtnCanUse = canUse;
        }
    }

    /// <summary>
    /// 触发开关逻辑 仅数据发生变化
    /// </summary>
    public void TriggerSwitchLogic()
    {
        if (GridType != EGridType.Wall)
            return;
        if (!CommonParam.SwitchColorList.Contains(GridColor))
            return;

        switch (GridDirection)
        {
            case EDirection.Left:
                GridDirection = EDirection.Up;
                break;
            case EDirection.Up:
                GridDirection = EDirection.Right;
                break;
            case EDirection.Right:
                GridDirection = EDirection.Down;
                break;
            case EDirection.Down:
                GridDirection = EDirection.Left;
                break;
            case EDirection.LeftRight:
                GridDirection = EDirection.UpDown;
                break;
            case EDirection.UpDown:
                GridDirection = EDirection.LeftRight;
                break;
            case EDirection.LeftUp:
                GridDirection = EDirection.UpRight;
                break;
            case EDirection.UpRight:
                GridDirection = EDirection.RightDown;
                break;
            case EDirection.RightDown:
                GridDirection = EDirection.DownLeft;
                break;
            case EDirection.DownLeft:
                GridDirection = EDirection.LeftUp;
                break;
        }

    }
}