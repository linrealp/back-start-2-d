﻿using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GridItem : MonoBehaviour
{
    /// <summary>
    /// 颜色sr序列
    /// </summary>
    [SerializeField]
    private SpriteRenderer[] squareSrs;

    /// <summary>
    /// 起始点sr
    /// </summary>
    [SerializeField]
    private SpriteRenderer startSr;

    /// <summary>
    /// 终点sr
    /// </summary>
    [SerializeField]
    private SpriteRenderer endSr;

    /// <summary>
    /// 条件sr
    /// </summary>
    [SerializeField]
    private SpriteRenderer targetSr;

    /// <summary>
    /// 单次开关sr序列
    /// </summary>
    [SerializeField]
    private SpriteRenderer[] singleSwitchBtnSrs;

    /// <summary>
    /// 复次开关sr序列
    /// </summary>
    [SerializeField]
    private SpriteRenderer[] pluralSwitchBtnSrs;

    /// <summary>
    /// 箭头脚本
    /// </summary>
    [SerializeField]
    private ArrowItem arrowItem;

    private CircleCollider2D cld;

    /// <summary>
    /// 格子数据
    /// </summary>
    private GridData gridData;

    /// <summary>
    /// 获取GridData
    /// </summary>
    public GridData GetGridData => gridData;

    [SerializeField, Tooltip("通道格子使用的颜色Index")]
    private int RoadIndex = 6;

    [SerializeField, Tooltip("普通墙格子使用的颜色Index")]
    private int NormalColorArrowIndex = 1;

    private void Awake()
    {
        cld = GetComponent<CircleCollider2D>();
        squareSrs[RoadIndex].sortingLayerName = "Ground";
    }

    /// <summary>
    /// 设置数据 并自动刷新
    /// </summary>
    /// <param name="data"></param>
    public void SetData(GridData data)
    {
        gridData = data;
        Refresh();
    }

    /// <summary>
    /// 依据数据刷新物体
    /// </summary>
    private void Refresh()
    {
        cld.enabled = gridData.GridType == EGridType.SwitchBtn || gridData.GridType == EGridType.Target ||
                           gridData.GridType == EGridType.StartOrEnd;

        switch (gridData.GridType)
        {
            case EGridType.None:
                SetAllShow(false);
                break;
            case EGridType.Road:
                RoadShowLogic();
                break;
            case EGridType.StartOrEnd:
                StartOrEndShowLogic();
                break;
            case EGridType.Target:
                TargetShowLogic();
                break;
            case EGridType.Wall:
                WallShowLogic();
                break;
            case EGridType.SwitchBtn:
                SwitchBtnShowLogic();
                break;
        }
    }

    /// <summary>
    /// 设置所有SquareSpriteRenderer的状态
    /// </summary>
    /// <param name="isShow">是否显示</param>
    private void SetAllSquareShow(bool isShow)
    {
        foreach (var sr in squareSrs)
        {
            sr.enabled = isShow;
        }
    }

    /// <summary>
    /// 设置所有单次开关 sr状态
    /// </summary>
    /// <param name="isShow">是否显示</param>
    private void SetAllSingleSwitchBtnShow(bool isShow)
    {
        foreach (var sr in singleSwitchBtnSrs)
        {
            if (sr == null)
                continue;
            sr.enabled = isShow;
        }
    }

    /// <summary>
    /// 设置所有复次开关 sr状态
    /// </summary>
    /// <param name="isShow">是否显示</param>
    private void SetAllPluralSwitchBtnShow(bool isShow)
    {
        foreach (var sr in pluralSwitchBtnSrs)
        {
            if (sr == null)
                continue;
            sr.enabled = isShow;
        }
    }

    /// <summary>
    /// 设置所有的Sr显示状态
    /// </summary>
    /// <param name="isShow"></param>
    private void SetAllShow(bool isShow)
    {
        SetAllSquareShow(isShow);
        SetAllSingleSwitchBtnShow(isShow);
        SetAllPluralSwitchBtnShow(isShow);
        startSr.enabled = isShow;
        endSr.enabled = isShow;
        targetSr.enabled = isShow;
        arrowItem.gameObject.SetActive(false);
    }

    /// <summary>
    /// 通道grid显示逻辑
    /// </summary>
    private void RoadShowLogic()
    {
        SetAllShow(false);
        squareSrs[RoadIndex].enabled = true;
    }

    /// <summary>
    /// 起始点/终点grid显示逻辑
    /// </summary>
    public void StartOrEndShowLogic()
    {
        SetAllShow(false);
        squareSrs[RoadIndex].enabled = true;
        startSr.enabled = !PlayerController.Instance.IsFinishCondition;
        endSr.enabled = PlayerController.Instance.IsFinishCondition;
    }

    /// <summary>
    /// 目标grid显示逻辑
    /// </summary>
    public void TargetShowLogic()
    {
        SetAllShow(false);
        targetSr.enabled = !gridData.IsTargetFinish;
        squareSrs[RoadIndex].enabled = true;
    }

    /// <summary>
    /// 墙显示逻辑
    /// </summary>
    private void WallShowLogic()
    {
        SetAllShow(false);
        if (gridData.GridDirection != EDirection.None) //有箭头
        {
            arrowItem.gameObject.SetActive(true);
            arrowItem.SetData(gridData.GridDirection);
            if (gridData.GridColor == EColor.None) //为有箭头无颜色的墙单独处理一个颜色，区分普通墙
            {
                squareSrs[NormalColorArrowIndex].enabled = true;
            }
            else
            {
                squareSrs[(int) gridData.GridColor].enabled = true;
            }
        }
        else //无箭头
        {
            squareSrs[(int) gridData.GridColor].enabled = true;
        }

        //为无色箭头墙单独做一个默认颜色
        if (gridData.GridDirection != EDirection.None && gridData.GridColor == EColor.None)
        {
            arrowItem.gameObject.SetActive(true);
            arrowItem.SetData(gridData.GridDirection);
        }
        else
        {
            squareSrs[(int) gridData.GridColor].enabled = true;
        }
    }

    /// <summary>
    /// 开关按钮显示逻辑
    /// </summary>
    private void SwitchBtnShowLogic()
    {
        SetAllShow(false);
        squareSrs[RoadIndex].enabled = true;
        switch (gridData.GridSwtichBtnType)
        {
            case ESwitchBtnType.Single:
                singleSwitchBtnSrs[(int) gridData.GridColor].enabled = true;
                break;
            case ESwitchBtnType.Plural:
                pluralSwitchBtnSrs[(int) gridData.GridColor].enabled = true;
                break;
        }
    }

    /// <summary>
    /// 箭头收到按钮触发的逻辑
    /// </summary>
    public void SwitchTriggerArrowChange()
    {
        Quaternion rotation = arrowItem.transform.localRotation;
        Vector3 targetRotation =
            new Vector3(rotation.eulerAngles.x, rotation.eulerAngles.y, rotation.eulerAngles.z - 90f);
        arrowItem.transform.DOLocalRotate(targetRotation, 0.1f).OnComplete(() =>
        {
            arrowItem.transform.localRotation = new Quaternion(0, 0, 0, rotation.w);
            Refresh();
        });
    }

    /// <summary>
    /// 触发开关总逻辑
    /// </summary>
    public void TriggerSwitchLogic()
    {
        EColor switchBtnGridColor = gridData.GridColor;
        if (!CommonParam.SwitchColorList.Contains(switchBtnGridColor))
            return;
        LevelManager.Instance.ColorGridListChange(switchBtnGridColor); //数据变化
        List<GridItem> gridItemList = LevelManager.Instance.GetColorGridItemList(switchBtnGridColor);
        foreach (var item in gridItemList)
        {
            item.SwitchTriggerArrowChange();
        }

        if (gridData.GridSwtichBtnType == ESwitchBtnType.Single)
        {
            gridData.SetBtnUse(false);
            SetAllSingleSwitchBtnShow(false);
        }
    }
}