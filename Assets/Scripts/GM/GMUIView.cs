﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GMUIView : MonoBehaviour
{
    public static bool OpenGM { get; private set; }

    public Slider xSlider;
    public Slider ySlider;
    public Slider zSlider;
    public Slider sizeSlider;
    public InputField inputField;
    public Button loadLevelBtn;
    public Button exitBtn;
    
    private void Awake()
    {
        OpenGM = true;
    }

    private void Start()
    {
        xSlider.onValueChanged.AddListener(OnXSliderChange);
        ySlider.onValueChanged.AddListener(OnYSliderChange);
        zSlider.onValueChanged.AddListener(OnZSliderChange);
        sizeSlider.onValueChanged.AddListener(OnSizeSliderChange);
        loadLevelBtn.onClick.AddListener(OnClickLoadLevelBtn);
        exitBtn.onClick.AddListener(OnClickExitBtn);
        Vector3 pos = Camera.main.transform.localPosition;
        xSlider.value = pos.x;
        ySlider.value = pos.y;
        zSlider.value = pos.z;
        sizeSlider.value = Camera.main.orthographicSize;

    }

    private void OnXSliderChange(float changeValue)
    {
        Vector3 pos = Camera.main.transform.localPosition;
        Camera.main.transform.localPosition = new Vector3(changeValue, pos.y, pos.z);
    }
    
    private void OnYSliderChange(float changeValue)
    {
        Vector3 pos = Camera.main.transform.localPosition;
        Camera.main.transform.localPosition = new Vector3(pos.x, changeValue, pos.z);
    }
    
    private void OnZSliderChange(float changeValue)
    {
        Vector3 pos = Camera.main.transform.localPosition;
        Camera.main.transform.localPosition = new Vector3(pos.x, pos.y, changeValue);
    }
    
    private void OnSizeSliderChange(float changeValue)
    {
        Camera.main.orthographicSize = changeValue;
    }

    private void OnClickLoadLevelBtn()
    {
        int level = Convert.ToInt32(inputField.text);
        LevelManager.Instance.LoadLevel(level);
    }

    private void OnClickExitBtn()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}