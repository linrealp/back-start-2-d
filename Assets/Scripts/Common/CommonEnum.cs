﻿using Unity.VisualScripting;

/// <summary>
/// 方向枚举
/// </summary>
public enum EDirection
{
    None = 0,

    /// <summary>
    /// 左
    /// </summary>
    Left = 1,

    /// <summary>
    /// 右
    /// </summary>
    Right = 2,

    /// <summary>
    /// 上
    /// </summary>
    Up = 3,

    /// <summary>
    /// 下
    /// </summary>
    Down = 4,

    /// <summary>
    /// 左右
    /// </summary>
    LeftRight = 5,

    /// <summary>
    /// 上下
    /// </summary>
    UpDown = 6,

    /// <summary>
    /// 左上
    /// </summary>
    LeftUp = 7,

    /// <summary>
    /// 上右
    /// </summary>
    UpRight = 8,

    /// <summary>
    /// 右下
    /// </summary>
    RightDown = 9,

    /// <summary>
    /// 下左
    /// </summary>
    DownLeft = 10,
}


public enum EColor
{
    None = 0,
    White = 1,
    Orange = 2,
    Pink = 3,
    Red = 4,
    Yellow = 5,
    Gray_NoLight = 6,
    Blue_NoLight = 7,
    Purple_NoLight = 8,
    Green_NoLight = 9,
}

/// <summary>
/// 格子类型枚举
/// </summary>
public enum EGridType
{
    None = -2,

    /// <summary>
    /// 通道
    /// </summary>
    Road = -1,

    /// <summary>
    /// 起点/终点
    /// </summary>
    StartOrEnd = 0,

    /// <summary>
    /// 目标物
    /// </summary>
    Target = 1,

    /// <summary>
    /// 墙
    /// </summary>
    Wall = 2,

    /// <summary>
    /// 机关按钮
    /// </summary>
    SwitchBtn = 3,
}


/// <summary>
/// 机关按钮类型
/// </summary>
public enum ESwitchBtnType
{
    None = 0,

    /// <summary>
    /// 单次开关
    /// </summary>
    Single,

    /// <summary>
    /// 复数次开关
    /// </summary>
    Plural,
}

/// <summary>
/// UI界面
/// </summary>
public enum EUIViewID
{
    /// <summary>
    /// 主菜单界面
    /// </summary>
    MainMenuView = 1,

    /// <summary>
    /// 设置界面
    /// </summary>
    SettingsView = 2,
    
    /// <summary>
    /// 选择关卡界面
    /// </summary>
    SelectLevelView = 3,
    
    /// <summary>
    /// 游戏中界面
    /// </summary>
    GameView = 4,
    
    /// <summary>
    /// 通关界面
    /// </summary>
    VictoryView = 5,
    
    /// <summary>
    /// 介绍界面
    /// </summary>
    IntroduceView = 6,
    
    /// <summary>
    /// 致谢界面
    /// </summary>
    ThanksView = 7,
    
    /// <summary>
    /// 隐私条款界面
    /// </summary>
    PrivacyView = 8,
}


/// <summary>
/// 关卡状态
/// </summary>
public enum ELevelStatus
{
    /// <summary>
    /// 锁定
    /// </summary>
    Lock = -1,

    /// <summary>
    /// 未锁定
    /// </summary>
    UnLock = 0,

    /// <summary>
    /// 通过
    /// </summary>
    Pass = 1,
}

/// <summary>
/// 事件枚举
/// </summary>
public enum EventType
{
    /// <summary>
    /// 关闭选择界面
    /// </summary>
    CloseSelectView,
    
    /// <summary>
    /// 关闭关卡管理器
    /// </summary>
    CloseLevelManager,
    
    /// <summary>
    /// 更新关卡文字
    /// </summary>
    UpdateLevelTxt,
}