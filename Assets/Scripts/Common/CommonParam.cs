﻿using System.Collections.Generic;
using UnityEngine;

public class CommonParam
{
    /// <summary>
    /// 颜色映射表
    /// </summary>
    public static Dictionary<string, EColor> ColorDict = new Dictionary<string, EColor>()
    {
        {"n", EColor.None},
        {"w", EColor.White},
        {"o", EColor.Orange},
        {"p", EColor.Pink},
        {"r", EColor.Red},
        {"y", EColor.Yellow},
        {"a", EColor.Gray_NoLight},
        {"e", EColor.Purple_NoLight},
        {"g", EColor.Green_NoLight},
        {"b", EColor.Blue_NoLight},
    };

    /// <summary>
    /// 方向映射表
    /// </summary>
    public static Dictionary<string, EDirection> DirectionDict = new Dictionary<string, EDirection>()
    {
        {"n", EDirection.None},
        {"l", EDirection.Left},
        {"r", EDirection.Right},
        {"u", EDirection.Up},
        {"d", EDirection.Down},
        {"lr", EDirection.LeftRight},
        {"ud", EDirection.UpDown},
        {"lu", EDirection.LeftUp},
        {"ur", EDirection.UpRight},
        {"rd", EDirection.RightDown},
        {"dl", EDirection.DownLeft},
    };

    /// <summary>
    /// 开关色
    /// </summary>
    public static List<EColor> SwitchColorList = new List<EColor>()
    {
        EColor.Orange,
        EColor.Pink,
        EColor.Red,
        EColor.Yellow,
        EColor.Green_NoLight,
        EColor.Blue_NoLight,
    };

    /// <summary>
    /// 单位
    /// </summary>
    public const float UnitVar = 1f;

    /// <summary>
    /// 无效果点
    /// </summary>
    public static Vector2 Vector2None = new Vector2(-1000f, -1000f);

    /// <summary>
    /// 最大递归次数
    /// </summary>
    public const int MaxRecursiveCnt = 10000;
}